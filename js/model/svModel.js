function SinhVien(
  _maSV,
  _tenSV,
  _emailSV,
  _passSV,
  _diemToan,
  _diemLy,
  _diemHoa
) {
  this.maSV = _maSV;
  this.tenSV = _tenSV;
  this.emailSV = _emailSV;
  this.passSV = _passSV;
  this.diemToan = _diemToan*1;
  this.diemLy = _diemLy*1;
  this.diemHoa = _diemHoa*1;
  this.tinhDTB = function () {
    return (this.diemHoa + this.diemLy + this.diemToan) / 3;
  };
}
