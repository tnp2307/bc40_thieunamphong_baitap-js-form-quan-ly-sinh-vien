function layThongTinForm() {
  var _maSV = document.getElementById("txtMaSV").value;
  var _tenSV = document.getElementById("txtTenSV").value;
  var _emailSV = document.getElementById("txtEmail").value;
  var _passSV = document.getElementById("txtPass").value;
  var _diemToan = document.getElementById("txtDiemToan").value * 1;
  var _diemLy = document.getElementById("txtDiemLy").value * 1;
  var _diemHoa = document.getElementById("txtDiemHoa").value * 1;
  var sv = new SinhVien(
    _maSV,
    _tenSV,
    _emailSV,
    _passSV,
    _diemToan,
    _diemLy,
    _diemHoa
  );
  return sv;
}

function renderds(svArr) {
  var contentHTML = "";
  console.log(svArr.length);
  for (var index = 0; index < svArr.length; index++) {
    var item = svArr[index];
    var contentTr = `<tr>
        <td>${item.maSV}</td>
        <td>${item.tenSV}</td>
        <td>${item.emailSV}</td>
        <td>${item.tinhDTB()}</td>
        <td>
        <button onclick="xoaSinhVien('${
          item.maSV
        }')" class="btn btn-danger">Xoá</button>
        <button onclick="suaSinhVien('${
          item.maSV
        }')" class="btn btn-warning">Sửa</button>
        </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function timKiemViTri(id, arr) {
  var viTri = -1;

  for (var index = 0; index < arr.length; index++) {
    var sv = arr[index];
    if (sv.maSV == id) {
      viTri = index;
      break;
    }
  }
  return viTri;
}
function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSV;
  document.getElementById("txtTenSV").value = sv.tenSV;
  document.getElementById("txtEmail").value = sv.emailSV;
  document.getElementById("txtPass").value = sv.passSV;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.dienLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}