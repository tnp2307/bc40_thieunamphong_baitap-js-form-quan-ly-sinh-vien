var dssv = [];
var DSSV = "DSSV";
var dssvJson = localStorage.getItem(DSSV);
  if (dssvJson != null) {
    var svArr = JSON.parse(dssvJson);
    dssv = svArr.map(function (item) {
      return new SinhVien(
        item.maSV,
        item.tenSV,
        item.emailSV,
        item.passSV,
        item.diemToan,
        item.diemLy,
        item.diemHoa,
      );
    });
  }
  renderds(dssv);

function themSinhVien() {
  var sv = layThongTinForm();
  dssv.push(sv);
  var dssvJson = JSON.stringify(dssv)
  localStorage.setItem(DSSV,dssvJson)  
  renderds(dssv);
}
function xoaSinhVien(idSv) {
  var viTri = timKiemViTri(idSv, dssv);
  console.log(viTri);
  if (viTri != -1) {
    dssv.splice(viTri, 1);
    renderds(dssv);
  }
}

function suaSinhVien(idSv) {
  var viTri = timKiemViTri(idSv, dssv);
  if (viTri == -1) {
    return;
  }
  var sv = dssv[viTri];
  showThongTinLenForm(sv);
}

function capNhatSinhVien() {
  var sv = layThongTinTuForm();
  var viTri = timKiemViTri(sv.ma, dssv);
  if (viTri != -1) {
    dssv[viTri] = sv;
    renderds(dssv);
  }
}